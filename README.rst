dj12 example project
====================

See `dj12 <https://gitlab.com/aiakos/dj12>`__ for more info.

To start the app run:

.. code-block:: sh

    docker-compose up --build

Admin login: http://localhost:8000/admin/ - root/root.
