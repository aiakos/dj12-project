from setuptools import setup, find_packages


with open('README.rst') as f:
    readme = f.read()


setup(
    name='dj12-project',
    version='0.0.0',
    description="Example dj12 project",
    long_description=readme,
    author="Aiakos Contributors",
    author_email="aiakos@aiakosauth.com",
    url="https://gitlab.com/aiakos/dj12-project/",
    keywords="dj12",

    install_requires=[
        'Django>=2.0,<3.0',
        'dj12',
        'gunicorn',
        'gevent',
        'whitenoise',
        'pyyaml',
    ],

    extras_require={
        'MySQL': ['mysqlclient'],
        'PostgreSQL': ['psycopg2'],
    },

    packages=find_packages(),
    include_package_data=True,

    zip_safe=True,
    classifiers=[]
)
